from typing import Any

from flask import Flask, jsonify, request
from flask_cors import CORS

from minessweeper import check_game_status, generate_game_id, initialize_board, mark_mines_as_exploded, reveal_cells

app = Flask(__name__)
CORS(app)
games = {}


@app.route("/api/new", methods=["POST"])
def new_game() -> Any:
    """Создает новую игру."""
    data = request.json
    width, height, mines_count = data["width"], data["height"], data["mines_count"]

    if width > 30 or height > 30 or mines_count > width * height - 1:
        return jsonify({"error": "Invalid parameters"}), 400

    game_id = generate_game_id()
    data = initialize_board(width, height, mines_count)
    display = data["display"]

    games[game_id] = data

    return jsonify(
        {
            "game_id": game_id,
            "width": width,
            "height": height,
            "mines-count": mines_count,
            "completed": False,
            "field": display,
        }
    )


@app.route("/api/turn", methods=["POST"])
def reveal() -> Any:
    """Обрабатывает ход игрока и возвращает состояние игры."""

    data = request.json
    game_id, row, col = data["game_id"], data["row"], data["col"]

    if game_id not in games:
        return jsonify({"error": "Invalid game_id"}), 400

    board = games[game_id]["board"]
    display = games[game_id]["display"]
    game_over = games[game_id]["completed"]

    if game_over:
        return jsonify({"error": "Game Over"}), 400

    if display[row][col] != " ":
        return jsonify({"error": "Cell already revealed"}), 400

    reveal_cells(board, display, row, col)
    check = check_game_status(board, display)

    if board[row][col] == "M":
        mark_mines_as_exploded(board, display)
        check = True

    if check:
        games[game_id]["completed"] = True

    return jsonify(
        {
            "game_id": game_id,
            "width": len(board[0]),
            "height": len(board),
            "mines_count": sum(row.count("M") for row in board),
            "field": board if check else display,
            "completed": check,
        }
    )


if __name__ == "__main__":
    app.run(debug=True)
