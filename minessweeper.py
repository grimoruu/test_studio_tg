import random
import uuid


def initialize_board(width: int, height: int, mines_count: int) -> dict:
    """
    Инициализирует игровое поле

    Parameters:
        width (int): Ширина игрового поля.
        height (int): Высота игрового поля.
        mines_count (int): Количество мин на поле.

    Returns:
        dict: Словарь, содержащий два списка: "board" (матрица с минами и числами)
              и "display" (матрица скрытого отображения для игрока).
    """
    board = [[" " for _ in range(width)] for _ in range(height)]
    display = [row[:] for row in board]

    mines = random.sample(range(width * height), mines_count)

    for mine in mines:
        row, col = divmod(mine, width)
        board[row][col] = "M"
    for row in range(height):
        for col in range(width):
            if board[row][col] == " ":
                mines_count = count_adjacent_mines(board, row, col)
                board[row][col] = str(mines_count) if mines_count > 0 else "0"
    return {"board": board, "display": display, "completed": False}


def count_adjacent_mines(board: list, row: int, col: int) -> int:
    """
    Подсчитывает количество мин вокруг ячейки.

    Parameters:
        board (list): Игровое поле.
        row (int): Номер строки ячейки.
        col (int): Номер столбца ячейки.

    Returns:
        int: Количество мин вокруг ячейки.
    """
    count = 0
    height, width = len(board), len(board[0])
    for i in range(max(0, row - 1), min(row + 2, height)):
        for j in range(max(0, col - 1), min(col + 2, width)):
            count += board[i][j] == "M"
    return count


def reveal_cells(board: list, display: list, row: int, col: int) -> None:
    """
    Рекурсивно открывает ячейки на игровом поле.

    Parameters:
        board (list): Игровое поле с минами и числами.
        display (list): Матрица скрытого отображения.
        row (int): Номер строки ячейки.
        col (int): Номер столбца ячейки.

    Returns:
        None
    """
    height, width = len(board), len(board[0])
    if 0 <= row < height and 0 <= col < width and display[row][col] == " ":
        display[row][col] = board[row][col]
        if board[row][col] == "0":
            for i in range(row - 1, row + 2):
                for j in range(col - 1, col + 2):
                    reveal_cells(board, display, i, j)


def check_game_status(board: list, revealed: list) -> bool:
    """
    Проверяет статус завершения игры.

    Parameters:
        board (list): Игровое поле с минами и числами.
        revealed (list): Матрица скрытого отображения.

    Returns:
        bool: True, если игра завершена, False в противном случае.
    """
    count = 0
    mines_count = 0
    for row in range(len(board)):
        for col in range(len(board[0])):
            if revealed[row][col] == " ":
                count += 1
                if board[row][col] == "M":
                    mines_count += 1
    return count == mines_count


def mark_mines_as_exploded(board: list, display: list) -> None:
    """
    Отмечает все мины как взорванные в матрице display.

    Parameters:
        board (list): Игровое поле с минами и числами.
        display (list): Матрица скрытого отображения.

    Returns:
        None
    """
    for i in range(len(display)):
        for j in range(len(display[0])):
            if board[i][j] == "M":
                board[i][j] = "X"


def generate_game_id() -> str:
    """
    Генерирует уникальный идентификатор игры.

    Returns:
        str: Уникальный идентификатор игры.
    """
    return str(uuid.uuid4())
