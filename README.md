# Minesweeper API

API для игры "Сапер".

## Установка и использование


1. Клонирование репозитория:

```bash
git clone https://gitlab.com/grimoruu/test_studio_tg.git
```
2. Директория проекта:
```bash
cd test_studio_tg
```
3. Установите зависимости poetry:
```bash
poetry install
```
4. Запуск приложения фласк:
```bash
python3 app.py
```

5. #### Веб-приложение будет доступно по адресу http://127.0.0.1:5000.
Для тестирования перейдите на [studiotg.ru](https://minesweeper-test.studiotg.ru/), введите ссылку на локальный сервер API (она внизу) в поле, предназначенное для установки URL сервера и начните играть.
```bash
http://127.0.0.1:5000/api
```